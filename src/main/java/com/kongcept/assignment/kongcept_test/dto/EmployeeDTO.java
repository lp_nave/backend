package com.kongcept.assignment.kongcept_test.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {

    private int id;

    private String name;

    private String Email;

    private String photo;

    private String address;

    private String password;

    private String branchName;

    private String bankName;
}
