package com.kongcept.assignment.kongcept_test.repositories;

import com.kongcept.assignment.kongcept_test.models.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo extends CrudRepository<Employee,Integer> {
    Employee findByEmpEmail (String email);
}
