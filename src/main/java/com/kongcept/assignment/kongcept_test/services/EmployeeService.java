package com.kongcept.assignment.kongcept_test.services;

import com.kongcept.assignment.kongcept_test.dto.EmployeeDTO;
import com.kongcept.assignment.kongcept_test.dto.LoginDTO;
import com.kongcept.assignment.kongcept_test.models.Employee;
import com.kongcept.assignment.kongcept_test.repositories.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeRepo employeeRepo;

    public ResponseEntity<?> getAllEmployees() throws Exception {
        try{
            List<Employee> rawlist = (List<Employee>) employeeRepo.findAll();

            List<EmployeeDTO> dtoList = new ArrayList<>();

            rawlist.forEach(employee -> {
                EmployeeDTO dto = new EmployeeDTO();
                dto.setId(employee.getEmp_id());
                dto.setAddress(employee.getEmp_address());
                dto.setEmail(employee.getEmpEmail());
                dto.setName(employee.getEmp_name());
                dto.setPhoto(employee.getEmp_photo());
                dto.setBankName(employee.getBranch().getBank().getBank_name());
                dto.setBranchName(employee.getBranch().getBranch_name());

                dtoList.add(dto);
            });

            return ResponseEntity.status(HttpStatus.OK).body(dtoList);
        }
        catch (Exception e){
//            return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body("Error");
            throw new Exception("Error", e);
        }
    }

}
