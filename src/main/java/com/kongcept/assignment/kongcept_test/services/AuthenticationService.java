package com.kongcept.assignment.kongcept_test.services;


import com.kongcept.assignment.kongcept_test.config.JwtTokenUtil;
import com.kongcept.assignment.kongcept_test.dto.JwtToken;
import com.kongcept.assignment.kongcept_test.dto.LoginDTO;
import com.kongcept.assignment.kongcept_test.models.Employee;
import com.kongcept.assignment.kongcept_test.repositories.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Service
@Transactional
public class AuthenticationService implements UserDetailsService {


    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Employee auth = employeeRepo.findByEmpEmail(email);
        if(auth== null){
            throw new UsernameNotFoundException("User not found with the email " + email);
        }
        return new org.springframework.security.core.userdetails.User( auth.getEmpEmail(),auth.getEmp_password(),
                new ArrayList<>());
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public ResponseEntity<?> authenticateUser(LoginDTO auth) throws Exception {
        authenticate(auth.getEmail(),auth.getPassword());
        Employee authentication = employeeRepo.findByEmpEmail(auth.getEmail());
            final UserDetails userDetails = loadUserByUsername(authentication.getEmpEmail());
            final String token =jwtTokenUtil.generateToken(userDetails);

            return ResponseEntity.ok(new JwtToken(token));
    }

}
