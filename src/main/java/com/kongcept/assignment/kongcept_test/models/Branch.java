package com.kongcept.assignment.kongcept_test.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "branch")
public class Branch {

    @Id
    @Column(name = "branch_id")
    private int branch_id;

    @Column(name = "branch_name")
    private String branch_name;

    @Column(name = "branch_address")
    private String branch_address;

    @ManyToOne
    @JoinColumn(name = "bank_bank_id", referencedColumnName = "bank_id")
    private Bank bank;

    @OneToMany(mappedBy = "branch", cascade = CascadeType.ALL)
    private List<Employee> employees;
}
