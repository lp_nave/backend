package com.kongcept.assignment.kongcept_test.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name="bank")
public class Bank {

    @Id
    @Column(name = "bank_id")
    private int bank_id;

    @Column(name = "bank_name")
    private String bank_name;

    @OneToMany(mappedBy = "bank", cascade = CascadeType.ALL)
    private List<Branch> branches;
}
