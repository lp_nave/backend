package com.kongcept.assignment.kongcept_test.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "employee")
public class Employee {

    @Id
    @Column(name = "emp_id")
    private int emp_id;

    @Column(name = "emp_name")
    private String emp_name;

    @Column(name = "empEmail")
    private String empEmail;

    @Column(name = "emp_photo")
    private String emp_photo;

    @Column(name = "emp_address")
    private String emp_address;

    @Column(name = "emp_password")
    private String emp_password;

    @ManyToOne
    @JoinColumn(name = "branch_branch_id", referencedColumnName = "branch_id")
    private Branch branch;
}
