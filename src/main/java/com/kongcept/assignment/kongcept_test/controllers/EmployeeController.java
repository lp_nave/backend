package com.kongcept.assignment.kongcept_test.controllers;

import com.kongcept.assignment.kongcept_test.dto.LoginDTO;
import com.kongcept.assignment.kongcept_test.services.AuthenticationService;
import com.kongcept.assignment.kongcept_test.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private AuthenticationService authenticationService;


//    @Autowired
//    public EmployeeController(EmployeeService employeeService) throws Exception{
//
//    }

    @GetMapping("/viewAllEmployees")
    public ResponseEntity<?> getAllemployees() throws Exception {
        return  employeeService.getAllEmployees();
    }

    @PostMapping("/emp_login")
    public ResponseEntity<?> authenticateEmployee (@RequestBody LoginDTO loginDTO) throws Exception {
        return authenticationService.authenticateUser(loginDTO);
    }
}
