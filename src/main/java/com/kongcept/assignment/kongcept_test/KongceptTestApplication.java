package com.kongcept.assignment.kongcept_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KongceptTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(KongceptTestApplication.class, args);
    }

}
